#!/bin/sh

# Setup hostname
echo $1 > /etc/hostname

# Generate locales (only en_US.UTF-8 for now)
sed -i -e '/en_US\.UTF-8/s/^# //g' /etc/locale.gen
locale-gen

# Change plymouth default theme
if plymouth-set-default-theme --list | grep -Fq 'mobian'; then    
  plymouth-set-default-theme mobian
fi

# Automatically start USB gadget on boot
systemctl unmask pinephone-usb-gadget.service
systemctl enable pinephone-usb-gadget.service

# Automatically enable tty on USB gadget
systemctl enable serial-getty@ttyGS0

